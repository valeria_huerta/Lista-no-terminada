package lista;
/**
 * Clase para excepciones de índices de lista inválidos.
 * Manejo de Datos 2019-1
 * Prof. Adrian Ulises Mercado Martinez
 * Ayudante: Diego Alberto Magallanes Ramirez
 *
 */
public class ExcepcionIndiceInvalido extends IndexOutOfBoundsException {

    /**
     * Constructor vacío.
     */
    public ExcepcionIndiceInvalido() {
        super();
    }

    /**
     * Constructor que recibe un mensaje para el usuario.
     * @param mensaje un mensaje que verá el usuario cuando ocurra la excepción.
     */
    public ExcepcionIndiceInvalido(String mensaje) {
        super(mensaje);
    }
}