package lista;
/**
 * Clase donde se ejecutara el programa.
 * Manejo de Datos 2019-1
 * Prof. Adrian Ulises Mercado Martinez
 * Ayudante: Diego Alberto Magallanes Ramirez
 *
 */
public class Main {
	public static void main(String [] args) {
		System.out.println("CREAMOS UNA LISTA:");
		Lista lista = new Lista();
		System.out.println("¿La lista es vacia? " + lista.esVacia());
		// R: True
		System.out.println("longitud: " + lista.getLongitud());
		// R: 0
		System.out.println("AGREGANDO ELEMENTOS ...");
		lista.agregaInicio(1);
		lista.agregaInicio(0);
		lista.agregaFinal(2);
		lista.agregaInicio(3);
		lista.agregaFinal(6);
		lista.agregaInicio(8);
		lista.agregaFinal(9);
		System.out.println("¿La lista es vacia? " + lista.esVacia());
		// R: False
		System.out.println("LA LISTA ES:");
		System.out.println(lista);
		// R: [8, 3, 0, 1, 2, 6, 9]
		System.out.println("longitud: " + lista.getLongitud());
		// 7
		System.out.println("ELIMINAMOS EL PRIMER ELEMENTO:");
		lista.eliminaPrimero();
		System.out.println(lista);
		// R: [3, 0, 1, 2, 6, 9]
		System.out.println("longitud: " + lista.getLongitud());
		// R: 6
		System.out.println("ELIMINAMOS EL ULTIMO ELEMENTO:");
		lista.eliminaUltimo();
		System.out.println(lista);
		// R: [3, 0, 1, 2, 6]
		System.out.println("longitud: " + lista.getLongitud());
		// R: 5
		System.out.println("¿LA LISTA CONTIENE AL CERO? " + lista.contiene(0));
		// R: True
		System.out.println("ELIMINAMOS EL 0 DE LA LISTA:");
		lista.elimina(0);
		System.out.println(lista);
		// R: [3, 1, 2, 6]
		System.out.println("longitud: " + lista.getLongitud());
		// R: 4
		System.out.println("¿LA LISTA CONTIENE AL CERO? " + lista.contiene(0));
		// R: False
		System.out.println("LA REVERSA DE LA LISTA ES: " + lista.reversa());
		// R: [6, 2, 1, 3]
		Lista copia = lista.copia();
		if(lista.equals(copia))
			System.out.println("SE COPIO EXITOSAMENTE");
		else
			System.out.println("ERROR AL COPIAR");
		// R: Se copio exitosamente.
		System.out.println("EL ELEMENTO EN LA POSICION 3 ES: " + lista.get(3));
		// R: 6
		System.out.println("EL ELEMENTO 2 ESTA EN LA POSICION: " + lista.getIndice(2));
		// R: 2
		System.out.println("EL PRIMER ELEMENTO ES: " + lista.getPrimero());
		// R: 3
		System.out.println("EL ULTIMO ELEMENTO ES: " + lista.getUltimo());
		// R: 6
	}
}