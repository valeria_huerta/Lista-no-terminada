package lista;
import java.util.NoSuchElementException;

/**
* Manejo de Datos 2019-1
* Prof. Adrian Ulises Mercado Martinez
* Ayudante: Diego Alberto Magallanes Ramirez
*
* Clase que representa una lista doblemente ligada.
*/
public class Lista {

	/* Clase Nodo privada para uso interno de la clase Lista. */
	private class Nodo {
		/* El elemento del nodo. */
		public int elemento;
		/* El nodo anterior. */
		public Nodo anterior;
		/* El nodo siguiente. */
		public Nodo siguiente;

		/* Construye un nodo con el elemento especificado. */
		public Nodo(int elemento) {
                        this.elemento = elemento;
		}
	} // Fin de la clase Nodo.

	/* Primer elemento de la lista. */
	private Nodo cabeza;
	/* Ultimo elemento de la lista. */
	private Nodo rabo;
	/* Numero de elementos en la lista. */
	private int longitud;

	/**
	 * Regresa la longitud de la lista
	 * @return la longitud de la lista, el numero de elementos que contiene.
	*/
	public int getLongitud() {
               return longitud;
	}

	/**
	 * Metodo que nos indica si la una lista esta vacia.
	 * @return <code>true</code> si la cabeza es nula, <code>false</code> e.o.c.
	*/
	public boolean esVacia() {
                if(this.cabeza == null) 
		return true;
                else return false;
	}

	/**
	 * Regresa el primer elemento de la lista.
	 * @return El primer elemento de la lista.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int getPrimero() {
		if(esVacia()) //no necesitamos crear objeto xq está dentro de la clase (no nec el this)
                    throw new NoSuchElementException("Esta lista es vacía."); //lanzar una nueva excepcion
                else
                    return cabeza.elemento;
	}

	/**
	 * Regresa el ultimo elemento de la lista.
	 * @return El ultimo elemento de la lista.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int getUltimo() {
                if(esVacia()) 
                    throw new NoSuchElementException("Esta lista es vacía."); //lanzar una nueva excepcion
                else
                    return rabo.elemento;
	}

	/**
	 * Agrega un elemento al final de la lista. Si la lista no tiene elementos,
	 * entonces el elemento a agregar sera el primero y el ultimo a la vez.
	 * @param elemento El elemento a agregar.
	*/
	public void agregaFinal(int elemento) {
		// TE TOCA, duda
                Nodo n = new Nodo (elemento);
                longitud ++;
                if(rabo==null){
                    cabeza = rabo = n;
                }
                else {
                    n.siguiente = null;
                    n.anterior = rabo;
                    rabo.siguiente = n;
                    rabo = n;
                }
	}

	/**
	 * Agrega un elemento al inicio de la lista. Si la lista no tiene elementos,
	 * entonces el elemento a agregar sera el primero y el ultimo a la vez.
	 * @param elemento El elemento a agregar
	*/
	public void agregaInicio(int elemento) {
		// TE TOCA, duda
                Nodo n = new Nodo (elemento);
                longitud ++;
                if(cabeza==null){
                    cabeza = rabo = n;
                }
                else {
                    n.anterior = null;
                    n.siguiente = cabeza;
                    cabeza.anterior = n;
                    cabeza = n;
                }
	}

	/**
	 * Elimina el primer elemento de la lista y lo regresa.
	 * @return El primer elemento de la lista antes de ser eliminado.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int eliminaPrimero() {
		// TE TOCA, duda  
                if(esVacia()) 
                    throw new NoSuchElementException("Esta lista es vacía."); 
                int temp = cabeza.elemento;
                if(cabeza==rabo){
                    cabeza = null;
                    rabo = null;
                }
                else{
                    cabeza.siguiente.anterior = null;
                    cabeza = cabeza.siguiente;
                }
                longitud --;
		return temp;
	}

	/**
	 * Elimina el ultimo elemento de la lista y lo regresa.
	 * @return El ultimo elemento de la lista antes de ser eliminado.
	 * @throws NoSuchElementException si la lista es vacia.
	*/
	public int eliminaUltimo() {
		// TE TOCA, duda
                if(esVacia()) 
                    throw new NoSuchElementException("Esta lista es vacía."); 
                int temp = rabo.elemento;
                if(cabeza==rabo){
                    cabeza = null;
                    rabo = null;
                }
                else{
                    rabo.anterior.siguiente = null;
                    rabo = rabo.anterior;
                }
                longitud --;
		return temp;
	}

	/**
	 * Elimina un elemento de la lista. Si el elemento no esta contenido en la
	 * lista, el metodo no hace nada. Si el elemento aparece varias veces en la
	 * lista, el metodo elimina la primera aparicion.
	 * @param elemento El elemento a eliminar.
	*/
	public void elimina(int elemento) {
            Nodo n = buscaNodo(cabeza, elemento); 
            if(n == null) return; //return quiere decir que termina.
            if(n.elemento == cabeza.elemento) eliminaPrimero();
            if(n.elemento == rabo.elemento) eliminaUltimo();
            else{
                n.anterior.siguiente = n.siguiente;
                n.siguiente.anterior = n.anterior;
                n = null;
            }  
            longitud --;
	}

	/* Metodo auxiliar para usarlo en el metodo contiene. */
	private Nodo buscaNodo(Nodo n, int elemento) {
            if (n == null) return null;
            if(n.elemento == elemento) return n;
            return buscaNodo( n.siguiente, elemento);		
	}

	/**
	 * Nos dice si un elemento esta en la lista.
	 * @param elemento El elemento que queremos saber si esta en la lista.
	 * @return <tt>true</tt> si <tt>elemento</tt> esta en la lista, <tt>false</tt> e.o.c.
	*/
	public boolean contiene(int elemento) {
		// TE TOCA    duda
                /**if(esVacia()) 
                    return false;
                Nodo temp = cabeza;
                while( temp != null && ! temp.equals(temp.elemento))
                    temp = temp.siguiente;
                return temp != null;
                */
                return buscaNodo(cabeza,elemento)!= null;
	}

	/**
	 * Regresa la reversa de la lista.
	 * @return Una nueva lista que es la reversa de la que manda a llamar al metodo.
	*/
	public Lista reversa() {
		// TE TOCA, duda
                Lista rev = new Lista();
                Nodo x = cabeza;
                while(x!=null){
                    rev.agregaInicio(x.elemento);
                    x = x.siguiente;
                }
                return rev;
	}

	/**
	 * Regresa una copia de la lista. La copia tiene los mismos elementos que la
	 * lista que manda a llamar el metodo, en el mismo orden.
	 * @return Una copia de la lista.
	*/
	public Lista copia() {
		// TE TOCA, duda
                //con agregaFinal
                Lista rev = new Lista();
                Nodo x = rabo;
                while(x!=null){
                    rev.agregaInicio(x.elemento);
                    x = x.anterior;
                }
                return rev;
	}

	/**
	 * Limpia la lista de elementos. El llamar este metodo es equivalente a eliminar
	 * todos los elementos de la lista.
	*/
	public void limpia() {
                cabeza = null;
                rabo = null;
                longitud = 0;
	}

	/**
	 * Regresa el i-esimo elemento de la lista.
	 * @param i El indice del elemento que queremos.
	 * @return El i-esimo elemento de la lista, si es mayor o igual a 0 y menor que 
	 * el numero de elementos en la lista.
	 * @throws ExcepcionIndiceInvalido si el indice recibido es menos que cero, o 
	 * mayor que el numero de elemntos en la lista menos uno.
	*/	
	public int get(int i) {
		// TE TOCA
		return 0;
	}

	/**
	 * Regresa el indice del elemento recibido.
	 * @param elemento El elemento del que se busca el indice.
	 * @return El indice del elemento recibido, o -1 si el elemento no esta contenido en la lista.
	*/
	public int getIndice(int elemento) {
		// TE TOCA, duda
		return elemento;
	}

	/* Metodo auxiliar para getIndice. */
	private int indice(int elemento, Nodo nodo, int i) {
		// TE TOCA 
                //es como busca nodo, se usa en contiene y elimina
                //declarar variable que empiece en cero, si el elemento que queremos encontrar esta en cabeza, regresar cero
                //while 
                //hacer temp de nodo 
                //for desde 0 hasta longitud, si lo encuentra regresa indice, sino no regresa nada
		return 0;
	}

	/**
	 * Nos dice si la lista es igual al objeto recibido.
	 * @param o El objeto con el que hay que comparar.
	 * @return <tt>true</tt> si la lista es igual al objeto recibido;
	 * <tt>false</tt> e.o.c.
	*/
	@Override public boolean equals(Object o) {
		if(o == null)
			return false;
		if(getClass() != o.getClass())
			return false;
		@SuppressWarnings("unchecked") Lista nueva = getClass().cast(o);
		if(longitud != nueva.longitud)
			return false;
		Nodo nodo1 = cabeza;
		Nodo nodo2 = nueva.cabeza;
		while(nodo1 != null) {
			if(!(nodo1.elemento == nodo2.elemento))
				return false;
			nodo1 = nodo1.siguiente;
			nodo2 = nodo2.siguiente;
		}
		return true;
	}

	/**
	 * Regresa una representacion en cadena de la lista.
	 * @return Una representacion en cadena de la lista.
	*/
	@Override public String toString() {
		String s = "[";
		if(longitud == 0)
			return s += "]";
		Nodo auxiliar = cabeza;
		while(auxiliar.siguiente != null) {
			s += String.format("%S, ", auxiliar.elemento);
			auxiliar = auxiliar.siguiente;
		}
		s += String.format("%S]", rabo.elemento);
		return s;
	}
}